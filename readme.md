# OAuth Sample Code

This repository contains simple programs demonstrating how to make use of OAuth-based user impersonation in Atlassian Connect.
Refer to the following docs for more details:

* [Jira Cloud](https://developer.atlassian.com/cloud/jira/software/user-impersonation-for-connect-apps/)
* [Confluence Cloud](https://developer.atlassian.com/cloud/confluence/user-impersonation-for-connect-apps/)

## Pre-requisites

You will need an add-on with the `ACT_AS_USER` scope, and access to that add-on's installation data (so you can retrieve
the secret and `oauthClientId` to provide to the sample program).

## Contents

### `java`

A standalone java program demonstrating how to obtain and use a token from the authorization service. To install and run (requires JDK 8):

```
cd java
javac OAuthTokenGetter.java
java OAuthTokenGetter [arguments]
```

See the comments in `OAuthTokenGetter.java` or run the class without arguments for more information.

### `javascript`

A javascript program also demonstrating how to obtain and use a token. It requires node and npm. To install and run (requires node >= v0.12.2:

```
cd javascript
npm start -- \
  --oauthClientId "YOUR_APP_CLIENT_ID" \
  --instanceBaseUrl "YOUR_SITE_URL" \
  --secret "YOUR_APP_SHARED_SECRET" \
  --userAccountId "TARGET_AAID"
```

See the comments at the top of `index.js` or run the program without arguments for more information.
